import React from "react"

import Layout from '@bit/jauyeunggithub.landing-page.layout';
import SEO from "../components/seo"

import LandingPage from '@bit/jauyeunggithub.landing-page.landing-page';
import SubscribeButton from "../components/subscribe-button";

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <LandingPage />
    <div className='center'>
      <SubscribeButton />
    </div>
  </Layout>
)

export default IndexPage
